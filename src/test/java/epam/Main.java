package epam;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scr = new Scanner(System.in);
        int from = scr.nextInt();
        int until = scr.nextInt();
        List<Integer> list = new ArrayList<Integer>();

        Number.addNumbersToList(from,until,list);
        Number.printEvenNumber(list);
        Number.printOddNumbers(list);
        System.out.println("Sum of odd numbers: " + Number.sumOddNumbers());
        System.out.println("Sum of even numbers: " + Number.sumOfEvenNumbers());

        int F1 = Number.theBiggestOddNumber(list);
        int F2 = Number.theBiggestEvenNumber(list);
        System.out.println("The biggest odd number is: " + F1);
        System.out.println("The biggest even number is: " + F2);

        Fibonacci.percentageOfOddNumbers(F1,F2,scr.nextInt());
    }
}


