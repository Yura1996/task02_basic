package epam;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
public class Number {
    private static int sumOfOddNumbers = 0;
    private static int sumOfEvenNumbers = 0;



    public static void addNumbersToList (int from, int until, List<Integer> list) {
        for (int i = from; i <= until; i++) {
            list.add(i);
        }
    }

    /**print ll odd numbers from lowest to highest
     *
     * @param list
     */
    public static void printOddNumbers(List<Integer> list) {
        System.out.print("Odd numbers: ");
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) % 2 != 0) {
                System.out.print(list.get(i) + " ");
                sumOfOddNumbers += list.get(i);
            }
        }
        System.out.println();
    }


    //print all even numbers from hightest to lowest
    public static void printEvenNumber(List<Integer> list) {

        System.out.print("Even numbers: ");
        for (int i = list.size() - 1; i >= 0; i--) {
            if (list.get(i) % 2 == 0) {
                System.out.print(list.get(i) + " ");
                sumOfEvenNumbers += list.get(i);
            }
        }
        System.out.println();
    }

    //print the biggest odd number
    public static int theBiggestOddNumber (List<Integer> list) {
        Collections.sort(list);
        int lastOddNum = list.get(list.size() - 1);
        int prevLastOddNum = list.get(list.size() - 2);
        if (lastOddNum % 2 != 0) {
            return lastOddNum;
        }else {
            return prevLastOddNum;
        }
    }

    //print the biggest even number
    public static int theBiggestEvenNumber (List<Integer> list) {
        Collections.sort(list);
        int lastEvenNum = list.get(list.size() - 1);
        int prevLastEvenNum = list.get(list.size() - 2);
        if (lastEvenNum % 2 == 0) {
            return lastEvenNum;
        } else {
            return prevLastEvenNum;
        }
    }


    public static int sumOddNumbers() {
        return sumOfOddNumbers;
    }


    public static int sumOfEvenNumbers() {
        return sumOfEvenNumbers;
    }
}



